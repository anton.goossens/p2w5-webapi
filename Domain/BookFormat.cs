﻿namespace EReader.BL.Domain;

public enum BookFormat : byte
{
    EPUB = 1,
    PDF,
    TXT,
    CBR
}
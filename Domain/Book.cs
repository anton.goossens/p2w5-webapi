﻿using System.ComponentModel.DataAnnotations;

namespace EReader.BL.Domain;

public class Book : IValidatableObject
{
    //public int Id { get; set; }
    //public int BookId { get; set; }
    [Key]
    public int Isbn { get; set; }
    
    [Required]
    [StringLength(128, MinimumLength = 5)]
    public string Title { get; set; }

    public BookFormat Format { get; set; }

    
    // x-op-(0..)1
    //[Required] // = x-op-1 (ipv x-op-0..1)
    public Publisher Publisher { get; set; }
    // x-op-n
    // n-op-n scenarion(3)
    public ICollection<Author> Authors { get; set; } 
    // n-op-n scenarion(2): naam kan ook gewoon 'Authors' blijven
    public ICollection<BookAuthor> BookAuthors { get; set; } 
    // n-op-n scenarion(1) (theoretisch)
    public ICollection<Contribution> Contributions { get; set; }
    

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        List<ValidationResult> results = new List<ValidationResult>();

        if (!(Enum.IsDefined(Format)))
        {
            results.Add(new ValidationResult("Format bevat geen gedefinieerde waarde!", 
                new [] { nameof(Format)/*"Format"*/ }));
        }

        return results;
    }
}
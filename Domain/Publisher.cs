﻿using System.ComponentModel.DataAnnotations;

namespace EReader.BL.Domain;

public class Publisher
{
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    public int YearFounded { get; set; }
    
    //public string Address { get; set; }
    public Address Address { get; set; }
    
    public ICollection<Book> Books { get; set; }
}
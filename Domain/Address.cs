﻿//using Microsoft.EntityFrameworkCore; // EF-package in BL.Domain -> bad practise (N-Layer)!

namespace EReader.BL.Domain;

//[Owned]
public class Address
{
    public int Id { get; set; } // NIET bij Owned-types (= 1-op-1)
    
    public string Street { get; set; }
    public uint StreetNumber { get; set; }
    public ushort PostalCode { get; set; }
    public string City { get; set; }
    public Publisher Publisher { get; set; } // NIET bij Owned-types (= 1-op-1)
}
﻿namespace EReader.BL.Domain;

public class TextBook : Book
{
    public TextBook()
    {
        this.Format = BookFormat.TXT;
    }
}
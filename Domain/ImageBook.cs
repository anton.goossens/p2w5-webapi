﻿namespace EReader.BL.Domain;

public class ImageBook : Book
{
    public ImageBook()
    {
        this.Format = BookFormat.CBR;
    }
}
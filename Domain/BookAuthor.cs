﻿using System.ComponentModel.DataAnnotations;

namespace EReader.BL.Domain;

public class BookAuthor
{
    [Key]
    public Book Book { get; set; }
    [Key]
    public Author Author { get; set; }

    public byte Order { get; set; }
}
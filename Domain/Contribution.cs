﻿using System.ComponentModel.DataAnnotations;

namespace EReader.BL.Domain;

public class Contribution
{
    public int Id { get; set; }
    
    public DateTime Start { get; set; }
    public DateTime End { get; set; }
    public int NumberOfCharacters { get; set; }
    
    [Required]
    public Book Book { get; set; }
    [Required]
    public Author Author { get; set; }
}
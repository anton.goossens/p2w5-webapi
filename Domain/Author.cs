﻿using System.ComponentModel.DataAnnotations;

namespace EReader.BL.Domain;

public class Author
{
    public int Id { get; set; }
    
    [Required]
    public string Name { get; set; }

    public string SortName { get; set; }
    public DateTime DateOfBirth { get; set; }

    // x-op-n
    // n-op-n scenario (3)
    public ICollection<Book> Books { get; set; }
    // n-op-n scenario (2) naam kan ook gewoon 'Books' blijven
    public ICollection<BookAuthor> BookAuthors { get; set; } 
    // n-op-n scenario (1) theoretisch
    public ICollection<Contribution> Contributions { get; set; } 
}
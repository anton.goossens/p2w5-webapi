﻿using EReader.BL.Domain;

namespace EReader.DAL.EF;

internal static class BookInitializer
{
    private static bool _hasRunInitialization = false;
    public static void Initialize(BookDbContext context, bool rebuildDatabase)
    {
        if (_hasRunInitialization)
            return;
        
        if (rebuildDatabase)
            context.Database.EnsureDeleted();

        if (context.Database.EnsureCreated())
            Seed(context);

        _hasRunInitialization = true;
    }

    private static void Seed(BookDbContext context)
    {
        #region Publishers
        var allenUnwin = new Publisher
        {
            Name = "Allen & Unwin",
            YearFounded = 1976,
            Address = new Address() {Street = "Alexander St", StreetNumber = 83, PostalCode = 2065, City = "Crows Nest"},
            Books = new List<Book>()
        };
        context.Publishers.Add(allenUnwin);
        #endregion

        #region Authors
        var tolkien = new Author
        {
            Name = "J. R. R. Tolkien",
            DateOfBirth = new DateTime(1892, 1, 3),
            Books = new List<Book>()
        };
        var king = new Author
        {
            Name = "Stephen King",
            DateOfBirth = new DateTime(1947, 9, 21),
            Books = new List<Book>()
        };
        var straub = new Author
        {
            Name = "Peter Straub",
            DateOfBirth = new DateTime(1943, 3, 2),
            Books = new List<Book>()
        };
        context.Authors.Add(tolkien);
        context.Authors.Add(king);
        context.Authors.Add(straub);
        #endregion

        #region Books (-Publisher)
        var fellowShip = new Book
        {
            Title = "The Fellowship of the Ring",
            Publisher = allenUnwin,
            Authors = new List<Author>(),
        };
        var twoTowers = new Book
        {
            Title = "The Two Towers",
            Publisher = allenUnwin,
            Authors = new List<Author>(),
        };
        var talisman = new Book
        {
            Title = "The Talisman",
            Authors = new List<Author>(),
        };
        context.Books.Add(fellowShip);
        context.Books.Add(twoTowers);
        context.Books.Add(talisman);
        #endregion
        
        #region Books-Authors
        fellowShip.Authors.Add(tolkien);
        tolkien.Books.Add(fellowShip);

        twoTowers.Authors.Add(tolkien);
        tolkien.Books.Add(twoTowers);
        
        talisman.Authors.Add(king);
        talisman.Authors.Add(straub);
        king.Books.Add(talisman);
        straub.Books.Add(talisman);
        #endregion
        // #region BookAuthors
        // var fellowshipTolkien = new BookAuthor
        // {
        //     Book = fellowShip,
        //     Author = tolkien
        // };
        // var twoTowersTolkien = new BookAuthor
        // {
        //     Book = twoTowers,
        //     Author = tolkien
        // };
        // var talismanKing = new BookAuthor
        // {
        //     Book = talisman,
        //     Author = king
        // };
        // var talismanStraub = new BookAuthor
        // {
        //     Book = talisman,
        //     Author = straub
        // };
        // context.BookAuthors.Add(fellowshipTolkien);
        // context.BookAuthors.Add(twoTowersTolkien);
        // context.BookAuthors.Add(talismanKing);
        // context.BookAuthors.Add(talismanStraub);
        // #endregion


        context.SaveChanges();

        // Detach all entities from the context,
        // else this data stays attached to context and should be read from the db when needed!
        context.ChangeTracker.Clear();
    }
}
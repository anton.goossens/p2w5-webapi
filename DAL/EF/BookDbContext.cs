﻿using System.Diagnostics;
using System.Runtime.CompilerServices;
using EReader.BL.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace EReader.DAL.EF;

public class BookDbContext : DbContext
{
    public DbSet<Book> Books { get; set; }
    public DbSet<TextBook> TextBooks { get; set; }
    public DbSet<ImageBook> ImageBooks { get; set; }
    public DbSet<Author> Authors { get; set; }
    public DbSet<Publisher> Publishers { get; set; }

    // public BookDbContext()
    // {
    //     this.Database.EnsureDeleted();
    //     this.Database.EnsureCreated();
    // }
    public BookDbContext(DbContextOptions options) : base(options)
    {
        BookInitializer.Initialize(this, rebuildDatabase: true);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);

        if (!optionsBuilder.IsConfigured)
            optionsBuilder.UseSqlite("Data Source=appdb.sqlite");

        // Logging (sql-statements)
        //optionsBuilder.LogTo(Console.WriteLine);
        //#if DEBUG
            optionsBuilder.LogTo(message => Debug.WriteLine(message), LogLevel.Information);
        //#endif
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        /* alternatief om entiteit kenbaar te maken als onderdeel van het conceptueel model,
           indien aparte DbSet-props niet noodzakelijk zijn */
        //modelBuilder.Entity<TextBook>();
        //modelBuilder.Entity<ImageBook>();

        /* tabel naam (= standaard naam van de DbSet-property) */
        //modelBuilder.Entity("Book").ToTable("tblBook");
        //modelBuilder.Entity(nameof(Book)).ToTable("tblBook"); // more typing-error proof!
        //modelBuilder.Entity(typeof(Book)).ToTable("tblBook");
        modelBuilder.Entity<Book>().ToTable("tblBooks");
        
        /* PK */
        //modelBuilder.Entity("Book").HasKey("Isbn");
        //modelBuilder.Entity(nameof(Book)).HasKey("Isbn");
        //modelBuilder.Entity(typeof(Book)).HasKey("Isbn");
        modelBuilder.Entity<Book>().HasKey(b => b.Isbn); // = most typing-errors proof! :-)
        
        /* kolom eigenschappen
           enkel toegepast indien ondersteund door database-type (en data-provider) */
        modelBuilder.Entity<Book>().Property(b => b.Title).HasMaxLength(128);
        
        
        /* Relaties */
        // (0..)1-op-n: Publisher - Book
        modelBuilder.Entity<Publisher>()
            .HasMany(p => p.Books)
            .WithOne(b => b.Publisher)
            .IsRequired(false); // true (of leeg) -> 1!
        modelBuilder.Entity<Book>()
            .HasOne(b => b.Publisher)
            .WithMany(p => p.Books)
            .IsRequired(false);
        
        // (0..)1-op-0..1: Publisher - Address
        //modelBuilder.Entity<Address>().Property<int/*?*/>("FK_Publisher"); // optioneel expliciete declaratie FK
        modelBuilder.Entity<Publisher>()
            .HasOne(p => p.Address)
            .WithOne(a => a.Publisher)
            .HasForeignKey<Address>(/*"FK_Publisher"*/)
            .IsRequired(/*false*/);
        // 1-op-1 als Owned-type: Publisher - Address
        // modelBuilder.Owned<Address>();
        // modelBuilder.Entity<Publisher>()
        //     .OwnsOne(p => p.Address);
        
        // n-op-n
        // scenario (3): Book - Author
        modelBuilder.Entity<Book>()
            .HasMany(b => b.Authors)
            .WithMany(a => a.Books);
        
        // scenario (2): Book - BookAuthor - Author
        modelBuilder.Entity<BookAuthor>()
            .HasOne(ba => ba.Book)
            .WithMany(b => b.BookAuthors)
            .HasForeignKey("FK_Book")
            /*.IsRequired()*/;
        modelBuilder.Entity<BookAuthor>()
            .HasOne(ba => ba.Author)
            .WithMany(a => a.BookAuthors)
            .HasForeignKey("FK_Author")
            /*.IsRequired()*/;
        modelBuilder.Entity<BookAuthor>().HasKey(new string[] {"FK_Book", "FK_Author"});
        
        // scenario (1): Book - Contribution - Author
        modelBuilder.Entity<Book>()
            .HasMany(b => b.Authors)
            .WithMany(a => a.Books);
        
    }
}
﻿using EReader.BL.Domain;

namespace EReader.DAL.EF;

public class Repository : IStorage
{
    private readonly BookDbContext _ctx;

    // public Repository()
    // {
    //     _ctx = new BookDbContext();
    // }
    public Repository(BookDbContext bookDbContext)
    {
        _ctx = bookDbContext;
    }
    
    public IEnumerable<Book> ReadAllBooks()
    {
        return _ctx.Books.AsEnumerable();
        //return _ctx.Books;
        
        // bad implementation (geen gebruik van 'defered execution' gedrag)
        //return _ctx.Books.ToList();
    }

    public Book CreateBook(Book book)
    {
        _ctx.Books.Add(book);
        _ctx.SaveChanges();
        return book;
    }

    public Book ReadBook(int isbn)
    {
        return _ctx.Books.Find(isbn);
        //return _ctx.Books.Where(b => b.Isbn == isbn).First();
        //return _ctx.Books.First(b => b.Isbn == isbn);
        //return _ctx.Books.Where(b => b.Isbn == isbn).Single();
        //return _ctx.Books.Where(b => b.Isbn == 0).Single();
        //return _ctx.Books.Where(b => b.Isbn == 0).SingleOrDefault();

        // bad implementation -> in-memory (based on use of IEnumerable<T>)
        //var results = _ctx.Books.Where(delegate (Book b) { return b.Isbn == isbn; });
        //var results = _ctx.Books.AsEnumerable().Where(b => b.Isbn == isbn);
        //return results.SingleOrDefault();
    }

    public void UpdateBook(Book book)
    {
        _ctx.Books.Update(book);
        _ctx.SaveChanges();
    }
}
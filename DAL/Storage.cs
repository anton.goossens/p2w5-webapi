﻿using EReader.BL.Domain;

namespace EReader.DAL;

public class Storage : IStorage
{
    private static List<Book> _books;

    public Storage()
    {
        if (_books == null || _books.Any())
            Seed();
    }
    
    private void Seed()
    {
        TextBook cSharp = new TextBook() { Isbn = 1, Title = "C#", Format = BookFormat.PDF };
        TextBook java = new TextBook() { Isbn = 2, Title = "Java", Format = BookFormat.PDF };
        TextBook python = new TextBook() { Isbn = 3, Title = "Python" };

        _books = new List<Book>() {cSharp, java, python};
    }

    public IEnumerable<Book> ReadAllBooks()
    {
        return _books;
    }

    public Book CreateBook(Book book)
    {
        book.Isbn = _books.Max(b => b.Isbn) + 1;
        _books.Add(book);
        return book;
    }

    public Book ReadBook(int isbn)
    {
        return _books.Find(b => b.Isbn == isbn);
    }

    public void UpdateBook(Book book)
    {
        throw new NotImplementedException();
    }
}
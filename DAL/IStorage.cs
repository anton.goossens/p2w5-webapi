﻿using EReader.BL.Domain;

namespace EReader.DAL;

public interface IStorage
{
    IEnumerable<Book> ReadAllBooks();
    Book CreateBook(Book book);

    Book ReadBook(int isbn);
    void UpdateBook(Book book);
}
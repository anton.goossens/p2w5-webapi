﻿using EReader.BL.Domain;

namespace EReader.BL;

public interface IBookManager
{
    IEnumerable<Book> GetAllBooks();
    Book AddBook(string title, BookFormat format);
}
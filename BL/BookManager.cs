﻿using System.ComponentModel.DataAnnotations;
using EReader.BL.Domain;
using EReader.DAL;

namespace EReader.BL;

public class BookManager : IBookManager
{
    private IStorage _storage;
    public BookManager(IStorage storage)
    {
        //_storage = new Storage();
        _storage = storage;
    }
    public IEnumerable<Book> GetAllBooks()
    {
        return _storage.ReadAllBooks();
    }

    public Book AddBook(string title, BookFormat format)
    {
        Book newBook = new Book() {Title = title, Format = format};
        
        Validator.ValidateObject(newBook, new ValidationContext(newBook), validateAllProperties:true);

        return _storage.CreateBook(newBook);
    }

    private void Validate(Book book)
    {
        List<ValidationResult> results = new List<ValidationResult>();
        bool isValid = Validator.TryValidateObject(book, new ValidationContext(book), results,
            validateAllProperties: true);

        if (!isValid)
            throw new ValidationException(results[0], null, book);
    }
}
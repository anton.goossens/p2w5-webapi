﻿using System.ComponentModel.DataAnnotations;
using EReader.BL.Domain;

namespace EReader.UI.Web.Models;

public class AddBookViewModel
{
    [Required]
    [StringLength(128, MinimumLength = 5)]
    public string BookTitle { get; set; }
    public BookFormat FormatOfBook { get; set; }

    /*public DateOnly? DatePublished { get; set; }*/
    public DateTime? DatePublished { get; set; }
}
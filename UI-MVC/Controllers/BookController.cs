﻿using EReader.BL;
using EReader.BL.Domain;
using EReader.UI.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace EReader.UI.Web.Controllers;

// /Book
public class BookController : Controller
{
    private readonly IBookManager _bookManager;
    public BookController(IBookManager bookManager)
    {
        _bookManager = bookManager;
    }
    
    // GET /Book/Index
    public IActionResult Index()
    {
        // alle boeken tonen
        IEnumerable<Book> books = _bookManager.GetAllBooks();
        // (1) ViewData/ViewBag => sub-informatie
        //ViewBag.Books = books;
        // (2) ViewModel => hoofd-informatie
        return View(books);
    }

    // /Book/Add
    [HttpGet]
    public IActionResult Add()
    {
        return View();
    }
    [HttpPost]
    public IActionResult Add(AddBookViewModel book/*string title, BookFormat format*/)
    {
        bool isModelValid = ModelState.IsValid;
        if (!isModelValid)
            return View(book);
        
        // nieuw boek aanmaken
        Book createdBook = _bookManager.AddBook(book.BookTitle, book.FormatOfBook);
        
        //return View("AddConfirm", createdBook);
        return RedirectToAction("Index");
    }
}
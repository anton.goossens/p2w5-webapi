﻿using Microsoft.AspNetCore.Mvc;

namespace EReader.UI.Web.Controllers;

public class Test : Controller
{
    // GET https://...:.../Test/Index?key=value
    public string Index(int id, string key)
    {
        return $"{id}\t{key}";
    }
}
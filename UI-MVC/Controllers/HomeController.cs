﻿using System.Diagnostics;
using EReader.UI.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace EReader.UI.Web.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    // https://localhost:62153/HomeController/Index
    // https://localhost:62153/Home/Index/15?key=value
    // https://localhost:62153/Home
    // https://localhost:62153/
    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
    }
}
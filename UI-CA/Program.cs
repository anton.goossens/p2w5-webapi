﻿// See https://aka.ms/new-console-template for more information

using System.Text;
using EReader.BL;
using EReader.DAL;
using EReader.DAL.EF;
using EReader.UI.CA;
using Microsoft.EntityFrameworkCore;

// DI: composite root
//IStorage storage = new Storage();
// EF
//IStorage storage = new Repository();
// EF met configuratie
var optionsBuilder = new DbContextOptionsBuilder<BookDbContext>();
optionsBuilder.UseSqlite("Data Source=../../../appdb.sqlite");
var dbContext = new BookDbContext(optionsBuilder.Options);
IStorage storage = new Repository(dbContext);
IBookManager mgr = new BookManager(storage);

ConsoleUi consoleUi = new ConsoleUi(mgr, storage);
consoleUi.Run();

﻿using EReader.BL.Domain;

namespace EReader.UI.CA.Extensions;

public static class BookExtensions
{
    public static string Summary(this Book book)
    {
        return $"({book.Isbn}) Title: {book.Title}, [{book.Format}]";
    }
}
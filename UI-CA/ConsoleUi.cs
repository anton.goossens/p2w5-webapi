﻿using System.ComponentModel.DataAnnotations;
using EReader.BL;
using EReader.BL.Domain;
using EReader.DAL;
using EReader.UI.CA.Extensions;

namespace EReader.UI.CA;

public class ConsoleUi
{
    private IBookManager _bookManager;
    private IStorage _repo;

    public ConsoleUi(IBookManager bookManager, IStorage repo)
    {
        _bookManager = bookManager;
        _repo = repo;
    }

    public void Run()
    {
        // Create book
        Book newBook = new Book() {Title = "SomeTitle", Format = BookFormat.TXT};
        Book createdBook = _repo.CreateBook(newBook);
        
        // Show book
        Book book = _repo.ReadBook(createdBook.Isbn);
        Console.WriteLine(book.Summary());

        /* Query op gerelateerde data */
        // Show publisher of book
        
        // Show authors of book

        /* Laden van gerelateerde data */
        // Show book with publisher
        
        // Show books with Authors
        
        
        
        Console.ReadLine(); /*keep console open*/
    }
}